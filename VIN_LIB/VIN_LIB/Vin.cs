﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace VIN_LIB
{
    public class Vin
    {
        Dictionary<string, string> countries = new Dictionary<string, string>()
        {
            {"A[A-H]", "ЮАР" },
            {"A[J-N]", "Кот-д'Ивуар"},
            {"B[A-E]", "Ангола"},
            {"B[F-K]", "Кения"},
            {"B[L-R]", "Танзания"},
            {"C[A-E]", "Бенин"},
            {"C[F-K]", "Мадагаскар"},
            {"C[L-R]", "Тунис"},
            {"D[A-E]", "Египет"},
            {"D[F-K]", "Марокко"},
            {"D[L-R]", "Замбия"},
            {"E[A-E]", "Эфиопия"},
            {"E[F-K]", "Мозамбик"},
            {"F[A-E]", "Гана"},
            {"F[F-K]", "Нигерия"},
            {"J[A-T]", "Япония"},
            {"K[A-E]", "Шри Ланка"},
            {"K[F-K]", "Израиль"},
            {"K[L-R]", "Южная Корея"},
            {"K[S-Z0-9]", "Казахстан"},
            {"L[A-Z0-9]", "Китай"},
            {"M[A-E]", "Индия"},
            {"M[F-K]", "Индонезия"},
            {"M[L-R]", "Таиланд"},
            {"N[F-K]", "Пакистан"},
            {"N[L-R]", "Турция"},
            {"P[A-E]", "Филиппины"},
            {"P[F-K]", "Сингапур"},
            {"P[L-R]", "Малайзия"},
            {"R[A-E]", "ОАЭ"},
            {"R[F-K]", "Тайвань"},
            {"R[L-R]", "Вьетнам"},
            {"R[S-Z0-9]", "Саудовская Аравия"},
            {"S[A-M]", "Великобритания"},
            {"S[N-T]", "Германия"},
            {"S[U-Z]", "Польша"},
            {"S[1-4]", "Латвия"},
            {"T[A-H]", "Швейцария"},
            {"T[J-P]", "Чехия"},
            {"T[R-V]", "Венгрия"},
            {"T[W-Z1]", "Португалия"},
            {"U[H-M]", "Дания"},
            {"U[N-T]", "Ирландия"},
            {"U[U-Z]", "Румыния"},
            {"U[5-7]", "Словакия"},
            {"V[A-E]", "Австрия"},
            {"V[F-R]", "Франция"},
            {"V[S-W]", "Испания"},
            {"V[X-Z12]", "Сербия"},
            {"V[3-5]", "Хорватияя"},
            {"V[6-90]", "Эстония"},
            {"W[A-Z0-9]", "Германия"},
            {"X[A-E]", "Болгария"},
            {"X[F-K]", "Греция"},
            {"X[L-R]", "Нидерланды"},
            {"X[S-W]", "СССР/СНГ"},
            {"X[X-Z12]", "Люксембург"},
            {"X[1-30]", "Россия"},
            {"Y[A-E]", "Бельгия"},
            {"Y[F-K]", "Финляндия"},
            {"Y[L-R]", "Мальта"},
            {"Y[S-W]", "Швеция"},
            {"Y[X-Z0-9]", "Норвегия"},
            {"Y[3-5]", "Беларусь"},
            {"Y[6-90]", "Украина"},
            {"Z[A-R]", "Италия"},
            {"Z[X-Z12]", "Словения"},
            {"Z[3-5]", "Литва"},
            {"Z[6-90]", "Россия"},
            {"1[A-Z0-9]", "США"},
            {"2[A-Z0-9]", "Канада"},
            {"3[A-W]", "Мексика"},
            {"3[X-Z1-7]", "Коста Рика"},
            {"3[890]", "Каймановы острова"},
            {"4[A-Z0-9]", "США"},
            {"5[A-Z0-5]", "США"},
            {"6[A-W]", "Австралия"},
            {"7[A-E]", "Новая Зеландия"},
            {"8[A-E]", "Аргентина"},
            {"8[F-K]", "Чили"},
            {"8[L-R]", "Эквадор"},
            {"8[S-W]", "Перу"},
            {"8[X-Z12]", "Венесуэла"},
            {"9[A-E]", "Бразилия"},
            {"9[F-K]", "Колумбия"},
            {"9[L-R]", "Парагвай"},
            {"9[S-W]", "Уругвай"},
            {"9[X-Z12]", "Тринидад и Тобаго"},
            {"9[3-9]", "Бразилия"}
        };

        Dictionary<char, int> alphabet = new Dictionary<char, int>()
        {
            {'A', 1},
            {'B', 2},
            {'C', 3},
            {'D', 4},
            {'E', 5},
            {'F', 6},
            {'G', 7},
            {'H', 8},
            {'J', 1},
            {'K', 2},
            {'L', 3},
            {'M', 4},
            {'N', 5},
            {'P', 7},
            {'R', 9},
            {'S', 2},
            {'T', 3},
            {'U', 4},
            {'V', 5},
            {'W', 6},
            {'X', 7},
            {'Y', 8},
            {'Z', 9}
        };

        Regex countryRule = new Regex("");
        string[] yearCode = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "V", "W", "X", "Y" };
        int[] weights = {8, 7, 6, 5, 4, 3, 2, 10 , 0, 9, 8, 7 , 6, 5, 4, 3, 2};

        bool countryChecked = false;
        string vinBuffer;
        int checkSum = 0;
        int SM = 0;

        public bool CheckVIN(string vin)
        {
            checkSum = 0;
            if (Regex.IsMatch(vin, "^[A-HJ-NPR-Z0-9]{8}[0-9X][A-HJ-NPR-Y1-9][A-HJ-NPR-Z0-9]{3}[0-9]{4}") && vin.Length == 17)
            {
                foreach (KeyValuePair<string, string> el in countries)
                {
                    countryRule = new Regex("^" + el.Key);
                    if (countryRule.IsMatch(vin))
                    {
                        countryChecked = true;
                        break;
                    }
                }
                if (!countryChecked) return false;

                vinBuffer = vin;
                foreach (char el in vin)
                {
                    if (!Regex.IsMatch(el.ToString(), @"\d")) vinBuffer = vinBuffer.Replace(el, alphabet[el].ToString()[0]);
                }

                for (int i = 0; i < 17; i++)
                {
                    checkSum += Int32.Parse(vinBuffer[i].ToString()) * weights[i];
                }
                SM = checkSum % 11;
                if ((SM == 10 && vin[8] != 'X') || (SM != 10 && vin[8] == 'X')) return false;
                if (SM != Int32.Parse(vin[8].ToString())) return false;
                
            }
            else return false;

            return true;
        }

        public string GetVINCountry(string vin)
        {
            string country = "";
            foreach (KeyValuePair<string, string> el in countries)
            {
                countryRule = new Regex("^" + el.Key);
                if (countryRule.IsMatch(vin))
                {
                    country = el.Value;
                    break;
                }
            }
            return country;
        }

        public int GetTransportYear(string vin)
        {
            int year = 0;
            return year;
        }
    }
}
