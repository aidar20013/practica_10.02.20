﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;

namespace gibdd
{
    public partial class AddNew : Form
    {
        DateTime currTime = DateTime.UtcNow;
        SqlConnection con = new SqlConnection(@"Data Source = 303-11\SQLSERVER; Initial Catalog = gibdd; Integrated Security = true;");
        string imgPath = "imgs/";
        

        public AddNew()
        {
            InitializeComponent();
        }

        private void AddNew_Load(object sender, EventArgs e)
        {
            var cities = new AutoCompleteStringCollection();
            cities.AddRange(new string[]
            {
                "Москва",
                "Санкт-Петербург",
                "Уфа",
                "Самара",
                "Минск",
                "Ульяновск",
            });
            addresLifeCity.AutoCompleteCustomSource = cities;
            addressCity.AutoCompleteCustomSource = cities;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (currTime.AddMinutes(1) == DateTime.UtcNow)
            {
                this.Close();
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string name = "";
            string surname = "";
            string middlename = "";
            int passportserial = 0;
            int passportnumber = 0;
            string addres = "";
            string addresLife = "";
            string company = "";
            string job = "";
            string phone = "";
            string email = "";
            string photoPath = "";

            if (!Regex.IsMatch(emailText.Text, @"[\w\d]+@[\d\w]+[.][\d\w]+"))
                MessageBox.Show("email-адрес введён некорректно");
            if (nameText.Text.Length > 0) name = nameText.Text;
            if (surnameText.Text.Length > 0) surname = surnameText.Text;
            if (middlenameText.Text.Length > 0) middlename = middlenameText.Text;
            if (passportSerial.Text.Length > 0) passportserial = Convert.ToInt32(passportSerial.Text);
            if (passportNumber.Text.Length > 0) passportnumber = Convert.ToInt32(passportNumber.Text);
            if (address.Text.Length > 0 && addressCity.Text.Length > 0) addres = addressCity.Text + " " + address.Text;
            if (addressLife.Text.Length > 0 && addresLifeCity.Text.Length > 0) addresLife = addresLifeCity.Text + " " + addressLife.Text;
            if (companyname.Text.Length > 0) company = companyname.Text;
            if (jobname.Text.Length > 0) job = jobname.Text;
            if (phoneText.Text.Length > 0) phone = phoneText.Text;
            if (emailText.Text.Length > 0) email = emailText.Text;
            if (photo.Image != null) photoPath = openFileDialog1.SafeFileName;

            if (name == "" || surname == "" || middlename == "" || passportserial == 0 || passportnumber == 0 || addres == "" || addresLife == "" || phone == "" || email == "" || photoPath == "")
            {
                MessageBox.Show("Заполните все обязательные поля");
                return;
            }

            con.Open();
            SqlCommand com = new SqlCommand($"INSERT INTO drivers(name, surname, middlename, passport_serial, passport_number, address, address_life, company, jobname, phone, email, photo) " +
                                            $"VALUES ('{name}', '{surname}', '{middlename}', '{passportserial}', '{passportnumber}', '{addres}', '{addresLife}', '{company}', '{job}', '{phone}', '{email}', '{photoPath}')", con);
            if (com.ExecuteNonQuery() == 0) MessageBox.Show("Произошла ошибка");
            else MessageBox.Show("Добавлено");
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileInfo fileinfo = new FileInfo(openFileDialog1.FileName);
                if (fileinfo.Length <= 2097152)
                    if (Image.FromFile(openFileDialog1.FileName).Height / 4 == Image.FromFile(openFileDialog1.FileName).Width / 3)
                    {
                        if (!File.Exists(imgPath + openFileDialog1.SafeFileName)) File.Copy(openFileDialog1.FileName, imgPath + openFileDialog1.SafeFileName);
                        photo.Image = Image.FromFile(imgPath + openFileDialog1.SafeFileName);
                    }
                    else
                        MessageBox.Show("Изображение должно иметь формат 3 на 4");
                else
                    MessageBox.Show("Выбранный вами файл превышает допустимый размер (не более 2мб)");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
