﻿namespace esoft
{
    partial class AddNewJK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.nameText = new System.Windows.Forms.TextBox();
            this.costText = new System.Windows.Forms.TextBox();
            this.cityText = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusPlan = new System.Windows.Forms.RadioButton();
            this.statusBuild = new System.Windows.Forms.RadioButton();
            this.statusSell = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.complexText = new System.Windows.Forms.TextBox();
            this.errors = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button1.Location = new System.Drawing.Point(66, 423);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(8);
            this.button1.Size = new System.Drawing.Size(104, 42);
            this.button1.TabIndex = 0;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.button2.Location = new System.Drawing.Point(572, 423);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(8);
            this.button2.Size = new System.Drawing.Size(104, 42);
            this.button2.TabIndex = 1;
            this.button2.Text = "Закрыть";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // nameText
            // 
            this.nameText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.nameText.Location = new System.Drawing.Point(442, 101);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(234, 22);
            this.nameText.TabIndex = 2;
            // 
            // costText
            // 
            this.costText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.costText.Location = new System.Drawing.Point(442, 129);
            this.costText.Name = "costText";
            this.costText.Size = new System.Drawing.Size(234, 22);
            this.costText.TabIndex = 3;
            // 
            // cityText
            // 
            this.cityText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cityText.Location = new System.Drawing.Point(442, 157);
            this.cityText.Name = "cityText";
            this.cityText.Size = new System.Drawing.Size(234, 22);
            this.cityText.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.statusSell);
            this.groupBox1.Controls.Add(this.statusBuild);
            this.groupBox1.Controls.Add(this.statusPlan);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.groupBox1.Location = new System.Drawing.Point(476, 238);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(200, 127);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Статус";
            // 
            // statusPlan
            // 
            this.statusPlan.AutoSize = true;
            this.statusPlan.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusPlan.Location = new System.Drawing.Point(11, 24);
            this.statusPlan.Name = "statusPlan";
            this.statusPlan.Size = new System.Drawing.Size(53, 18);
            this.statusPlan.TabIndex = 0;
            this.statusPlan.TabStop = true;
            this.statusPlan.Text = "План";
            this.statusPlan.UseVisualStyleBackColor = true;
            // 
            // statusBuild
            // 
            this.statusBuild.AutoSize = true;
            this.statusBuild.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusBuild.Location = new System.Drawing.Point(11, 47);
            this.statusBuild.Name = "statusBuild";
            this.statusBuild.Size = new System.Drawing.Size(110, 18);
            this.statusBuild.TabIndex = 1;
            this.statusBuild.TabStop = true;
            this.statusBuild.Text = "Строительство";
            this.statusBuild.UseVisualStyleBackColor = true;
            // 
            // statusSell
            // 
            this.statusSell.AutoSize = true;
            this.statusSell.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.statusSell.Location = new System.Drawing.Point(11, 70);
            this.statusSell.Name = "statusSell";
            this.statusSell.Size = new System.Drawing.Size(91, 18);
            this.statusSell.TabIndex = 2;
            this.statusSell.TabStop = true;
            this.statusSell.Text = "Реализация";
            this.statusSell.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(310, 93);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(8);
            this.label1.Size = new System.Drawing.Size(98, 30);
            this.label1.TabIndex = 10;
            this.label1.Text = "Название ЖК";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(63, 177);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(8);
            this.label2.Size = new System.Drawing.Size(345, 30);
            this.label2.TabIndex = 11;
            this.label2.Text = "Коэффициент добавочной стоимости на строительство";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(101, 121);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(8);
            this.label3.Size = new System.Drawing.Size(307, 30);
            this.label3.TabIndex = 12;
            this.label3.Text = "Затраты на строительство жилищного комплекса";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(351, 149);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(8);
            this.label4.Size = new System.Drawing.Size(57, 30);
            this.label4.TabIndex = 13;
            this.label4.Text = "Город";
            // 
            // complexText
            // 
            this.complexText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.complexText.Location = new System.Drawing.Point(442, 185);
            this.complexText.Name = "complexText";
            this.complexText.Size = new System.Drawing.Size(234, 22);
            this.complexText.TabIndex = 14;
            // 
            // errors
            // 
            this.errors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.errors.Location = new System.Drawing.Point(63, 270);
            this.errors.Name = "errors";
            this.errors.Padding = new System.Windows.Forms.Padding(8);
            this.errors.Size = new System.Drawing.Size(356, 95);
            this.errors.TabIndex = 15;
            // 
            // AddNewJK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 495);
            this.Controls.Add(this.errors);
            this.Controls.Add(this.complexText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cityText);
            this.Controls.Add(this.costText);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "AddNewJK";
            this.Text = "AddNewJK";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.TextBox costText;
        private System.Windows.Forms.TextBox cityText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton statusSell;
        private System.Windows.Forms.RadioButton statusBuild;
        private System.Windows.Forms.RadioButton statusPlan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox complexText;
        private System.Windows.Forms.Label errors;
    }
}