﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace esoft
{
    public partial class AddNewJK : Form
    {
        SqlConnection con = new SqlConnection("Data Source=303-11\\SQLSERVER;Initial Catalog=esoft;Integrated Security=true");

        public AddNewJK()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errors.Text = "";
            string name = "";
            string city = "";
            string plan = "";
            int cost = 0;
            int complex = 0;
            SqlCommand com = new SqlCommand($"INSERT INTO JK (ДОПИСАТЬ ПОЛЯ) VALUES ('{name}', '{cost}', '{city}', '{complex}', '{plan}')", con);

            if (nameText.Text.Length > 0) name = nameText.Text;
            else errors.Text += "Укажите название ЖК\n";
            if (costText.Text.Length > 0 && Convert.ToInt32(costText.Text) > 0) cost = Convert.ToInt32(costText.Text);
            else errors.Text += "Затраты на строительство должны быть больше 0\n";
            if (cityText.Text.Length > 0) city = cityText.Text;
            else errors.Text += "Укажите город в котором расположен ЖК\n";
            if (complexText.Text.Length > 0 && Convert.ToInt32(complexText.Text) > 0) complex = Convert.ToInt32(complexText.Text);
            else errors.Text += "Коэф. добавочной стоимости должен быть больше 0\n";

            foreach (RadioButton rb in groupBox1.Controls)
                if (rb.Checked == true) plan = rb.Text;

            if (errors.Text.Length == 0)
            {
                con.Open();
                if (com.ExecuteNonQuery() > 0) MessageBox.Show("Запись о жилищном комплексе добавлена.");
                else MessageBox.Show("Возникла ошибка при добавлении.");
                con.Close();
            }
        }
    }
}
